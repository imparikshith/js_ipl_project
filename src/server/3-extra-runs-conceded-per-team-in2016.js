const csvFilePathMatches = "../data/matches.csv";
const csvFilePathDeliveries = "../data/deliveries.csv";
const csv = require("csvtojson");
const fs = require("node:fs");
csv()
  .fromFile(csvFilePathMatches)
  .then((jsonObjMatches) => {
    // console.log(jsonObjMatches);

    csv()
      .fromFile(csvFilePathDeliveries)
      .then((jsonObjDeliveries) => {
        // console.log(jsonObjDeliveries);

        const matchIdsOf2016 = jsonObjMatches.reduce(function (acc, match) {
          if (match.season == "2016") {
            acc.push(match.id);
          }
          return acc;
        }, []);

        const extraRunsConcededPerTeamIn2016 = jsonObjDeliveries.reduce(
          function (acc, delivery) {
            if (matchIdsOf2016.includes(delivery.match_id)) {
              if (acc[delivery.bowling_team]) {
                acc[delivery.bowling_team] += parseInt(delivery.extra_runs);
              } else {
                acc[delivery.bowling_team] = parseInt(delivery.extra_runs);
              }
            }
            return acc;
          },
          {}
        );
        fs.writeFileSync(
          "../public/output/extraRunsConcededPerTeamIn2016.json",
          JSON.stringify(extraRunsConcededPerTeamIn2016)
        );
      });
  });