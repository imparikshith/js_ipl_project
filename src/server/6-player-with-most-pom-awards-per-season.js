const csvFilePathMatches = "../data/matches.csv";
const csv = require("csvtojson");
const fs = require("node:fs");
csv()
  .fromFile(csvFilePathMatches)
  .then((jsonObjMatches) => {
    // console.log(jsonObjMatches);

    const playerWithMostPoMAwardsPerSeason = Object.entries(
      Object.entries(
        jsonObjMatches.reduce(function (acc, match) {
          if (acc[match.season]) {
            if (acc[match.season][match.player_of_match]) {
              acc[match.season][match.player_of_match]++;
            } else {
              acc[match.season][match.player_of_match] = 1;
            }
          } else {
            acc[match.season] = {};
            acc[match.season][match.player_of_match] = 1;
          }
          return acc;
        }, {})
      ).reduce(function (acc, season) {
        acc[season[0]] = Object.fromEntries(
          Object.entries(season[1]).sort((a, b) => b[1] - a[1])
        );
        return acc;
      }, {})
    ).reduce(function (acc, season) {
      for (let player in season[1]) {
        acc[season[0]] = {};
        acc[season[0]][player] = season[1][player];
        return acc;
      }
    }, {});

    fs.writeFileSync(
      "../public/output/playerWithMostPoMAwardsPerSeason.json",
      JSON.stringify(playerWithMostPoMAwardsPerSeason)
    );
  });