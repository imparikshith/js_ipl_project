const csvFilePathMatches = "../data/matches.csv";
const csvFilePathDeliveries = "../data/deliveries.csv";
const csv = require("csvtojson");
const fs = require("node:fs");
csv()
  .fromFile(csvFilePathMatches)
  .then((jsonObjMatches) => {
    // console.log(jsonObjMatches);

    csv()
      .fromFile(csvFilePathDeliveries)
      .then((jsonObjDeliveries) => {
        // console.log(jsonObjDeliveries);

        const matchIdsOf2015 = jsonObjMatches.reduce(function (acc, match) {
          if (match.season == "2015") {
            acc.push(match.id);
          }
          return acc;
        }, []);

        const top10EconomicalBowlers = Object.entries(
          jsonObjDeliveries.reduce(function (acc, delivery) {
            if (matchIdsOf2015.includes(delivery.match_id)) {
              if (acc[delivery.bowler]) {
                acc[delivery.bowler].runs += parseInt(delivery.total_runs);
                if (
                  !(
                    parseInt(delivery.wide_runs) ||
                    parseInt(delivery.noball_runs)
                  )
                ) {
                  acc[delivery.bowler].balls++;
                }
              } else {
                acc[delivery.bowler] = {};
                acc[delivery.bowler].runs = parseInt(delivery.total_runs);
                if (
                  !(
                    parseInt(delivery.wide_runs) ||
                    parseInt(delivery.noball_runs)
                  )
                ) {
                  acc[delivery.bowler].balls = 1;
                } else {
                  acc[delivery.bowler].balls = 0;
                }
              }
            }
            return acc;
          }, {})
        )
          .reduce(function (acc, bowler) {
            acc.push([bowler[0], (bowler[1].runs / bowler[1].balls) * 6]);
            return acc;
          }, [])
          .sort((a, b) => a[1] - b[1])
          .filter(function (element, index) {
            if (index < 10) {
              return element;
            }
          });
        const top10EconomicalBowlersObj = Object.fromEntries(
          top10EconomicalBowlers
        );

        fs.writeFileSync(
          "../public/output/top10EconomicalBowlersIn2015.json",
          JSON.stringify(top10EconomicalBowlersObj)
        );
      });
  });