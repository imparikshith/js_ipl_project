const csvFilePathMatches = "../data/matches.csv";
const csvFilePathDeliveries = "../data/deliveries.csv";
const csv = require("csvtojson");
const fs = require("node:fs");
csv()
  .fromFile(csvFilePathMatches)
  .then((jsonObjMatches) => {
    // console.log(jsonObjMatches);

    csv()
      .fromFile(csvFilePathDeliveries)
      .then((jsonObjDeliveries) => {
        // console.log(jsonObjDeliveries);

        const matchIdsPerSeason = jsonObjMatches.reduce(function (acc, match) {
          if (acc[match.season]) {
            acc[match.season].push(match.id);
          } else {
            acc[match.season] = [match.id];
          }
          return acc;
        }, {});

        const strikeRateOfVKohliPerSeason = Object.entries(
          jsonObjDeliveries.reduce(function (acc, delivery) {
            if (delivery.batsman == "V Kohli") {
              for (let year in matchIdsPerSeason) {
                if (matchIdsPerSeason[year].includes(delivery.match_id)) {
                  if (acc[year]) {
                    acc[year].runs += parseInt(delivery.batsman_runs);
                    if (!parseInt(delivery.wide_runs)) {
                      acc[year].balls++;
                    }
                  } else {
                    acc[year] = {};
                    acc[year].runs = parseInt(delivery.batsman_runs);
                    if (!parseInt(delivery.wide_runs)) {
                      acc[year].balls = 1;
                    } else {
                      acc[year].balls = 0;
                    }
                  }
                }
              }
            }
            return acc;
          }, {})
        ).reduce(function (acc, season) {
          acc[season[0]] = (season[1].runs / season[1].balls) * 100;
          return acc;
        }, {});

        fs.writeFileSync(
          "../public/output/strikeRateOfVKohliPerSeason.json",
          JSON.stringify(strikeRateOfVKohliPerSeason)
        );
      });
  });