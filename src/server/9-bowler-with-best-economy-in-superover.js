const csvFilePath = "../data/deliveries.csv";
const csv = require("csvtojson");
const fs = require("node:fs");
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    // console.log(jsonObj);

    var flag = false;
    const bestBowlerInSuperOvers = Object.entries(
      jsonObj.reduce(function (acc, delivery) {
        if (parseInt(delivery.is_super_over)) {
          if (acc[delivery.bowler]) {
            acc[delivery.bowler].runs += parseInt(delivery.total_runs);
            if (
              !(parseInt(delivery.noball_runs) || parseInt(delivery.wide_runs))
            ) {
              acc[delivery.bowler].balls++;
            }
          } else {
            acc[delivery.bowler] = {};
            acc[delivery.bowler].runs = parseInt(delivery.total_runs);
            if (
              !(parseInt(delivery.noball_runs) || parseInt(delivery.wide_runs))
            ) {
              acc[delivery.bowler].balls = 1;
            } else {
              acc[delivery.bowler].balls = 0;
            }
          }
        }
        return acc;
      }, {})
    )
      .reduce(function (acc, bowler) {
        acc.push([bowler[0], (bowler[1].runs / bowler[1].balls) * 6]);
        return acc;
      }, [])
      .sort((a, b) => a[1] - b[1])
      .reduce(function (acc, bowler) {
        if (flag == false) {
          acc[bowler[0]] = bowler[1];
          flag = true;
        }
        return acc;
      }, {});

    fs.writeFileSync(
      "../public/output/bowlerWithBestEconomyInSuperOver.json",
      JSON.stringify(bestBowlerInSuperOvers)
    );
  });