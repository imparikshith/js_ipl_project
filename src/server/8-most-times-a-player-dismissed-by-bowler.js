const csvFilePath = "../data/deliveries.csv";
const csv = require("csvtojson");
const fs = require("node:fs");
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    // console.log(jsonObj);
    var flag = false;
    const mostTimesAPlayerDismissedByAnotherPlayer = Object.entries(
      jsonObj.reduce(function (acc, delivery) {
        if (delivery.batsman == "V Kohli") {
          if (
            delivery.player_dismissed &&
            delivery.dismissal_kind !== "run out"
          ) {
            if (acc[delivery.bowler]) {
              acc[delivery.bowler]++;
            } else {
              acc[delivery.bowler] = 1;
            }
          }
        }
        return acc;
      }, {})
    )
      .sort((a, b) => b[1] - a[1])
      .reduce(function (acc, bowler) {
        if (flag == false) {
          acc[bowler[0]] = bowler[1];
          flag = true;
        }
        return acc;
      }, {});

    fs.writeFileSync(
      "../public/output/mostTimesAPlayerDismissedByAnotherPlayer.json",
      JSON.stringify(mostTimesAPlayerDismissedByAnotherPlayer)
    );
  });