const csvFilePathMatches = "../data/matches.csv";
const csv = require("csvtojson");
const fs = require("node:fs");
csv()
  .fromFile(csvFilePathMatches)
  .then((jsonObjMatches) => {
    console.log(jsonObjMatches);

    const noOfTimesTeamWonTossAndMatch = jsonObjMatches.reduce(function (
      acc,
      match
    ) {
      if (match.toss_winner == match.winner) {
        if (acc[match.winner]) {
          acc[match.winner]++;
        } else {
          acc[match.winner] = 1;
        }
      }
      return acc;
    },
    {});

    fs.writeFileSync(
      "../public/output/noOfTimesTeamWonTossAndMatch.json",
      JSON.stringify(noOfTimesTeamWonTossAndMatch)
    );
  });