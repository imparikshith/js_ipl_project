const csvFilePath = "../data/matches.csv";
const csv = require("csvtojson");
const fs = require("node:fs");
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    // console.log(jsonObj);

    const matchesWonPerTeamPerYear = jsonObj.reduce(function (acc, match) {
      if (acc[match.season]) {
        if (acc[match.season][match.winner]) {
          acc[match.season][match.winner]++;
        } else {
          acc[match.season][match.winner] = 1;
        }
      } else {
        acc[match.season] = {};
        acc[match.season][match.winner] = 1;
      }
      return acc;
    }, {});

    fs.writeFileSync(
      "../public/output/matchesWonPerTeamPerYear.json",
      JSON.stringify(matchesWonPerTeamPerYear)
    );
  });